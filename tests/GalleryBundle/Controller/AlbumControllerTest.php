<?php

namespace GalleryBundle\Tests\Controller;

use GalleryBundle\Controller\AlbumController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AlbumControllerTest extends WebTestCase
{

    public function testIndex_code_200()
    {
        $client = $this->createClient();
        $client->request('GET', 'api/albums');
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

    }

    public function testIndex_isJson()
    {
        $client = $this->createClient();
        $client->request('GET', 'api/albums');
        $response = $client->getResponse();

        $this->assertJson($response->getContent());
    }

    public function testShow_code_200()
    {
        $client = $this->createClient();
        $client->request('GET', 'api/albums/2/page/2');
        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testShow_code_404()
    {
        $client = $this->createClient();
        $client->request('GET', 'api/albums/0/page/1');
        $response = $client->getResponse();

        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testShow_isJson()
    {
        $client = $this->createClient();
        $client->request('GET', 'api/albums/4/page/2');
        $response = $client->getResponse();

        $this->assertJson($response->getContent());
    }

    public function testPagination_code_200()
    {
        $client = $this->createClient();
        $client->request('GET', 'api/albums/3/pagination');

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPagination_code_404()
    {
        $client = $this->createClient();
        $client->request('GET', 'api/albums/0/pagination');

        $response = $client->getResponse();

        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testPagination_notEmpty()
    {
        $client = $this->createClient();
        $client->request('GET', 'api/albums/2/pagination');
        $response = $client->getResponse();

        $this->assertNotEmpty($response->getContent());
    }

    public function testPagination_isEmpty()
    {
        $client = $this->createClient();
        $client->request('GET', 'api/albums/1/pagination');
        $response = $client->getResponse();
        $this->assertEmpty($response->getContent());
    }

}
