
PhotoGallery.module "Entities", (Entities, PhotoGallery, Backbone, Marionette, $, _) ->

    Entities.Album = Backbone.Model.extend
        url : ->
            '/api/albums/' + @id + '/page/' + @attributes.page
        defaults:
            title: ''
            description: ''
            createdAt: ''

    Entities.AlbumCollection = Backbone.Collection.extend
        url: '/api/albums'
        model: Entities.Album
        comparator: (album) ->
            album.get 'title' + " " + album.get 'description'

    API =
        getAlbumEntities: ->
            albums = new Entities.AlbumCollection()
            new Promise (resolve) ->
                albums.fetch
                    success: (data) ->
                        resolve data
                        undefined
                    error: ->
                        resolve undefined
                        undefined
                undefined
        getAlbumEntity: (id, page) ->
            album = new Entities.Album
                id: id
                page: page
            new Promise (resolve) ->
                album.fetch
                    success: (data) ->
                        data.attributes.page = page
                        resolve data
                        undefined
                    error: ->
                        resolve undefined
                        undefined
                undefined

    PhotoGallery.reqres.setHandler 'album:entities', ->
        API.getAlbumEntities()

    PhotoGallery.reqres.setHandler 'album:entity', (id, page) ->
        API.getAlbumEntity id, page

    undefined

