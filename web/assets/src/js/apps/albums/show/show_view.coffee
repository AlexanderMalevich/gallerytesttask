
PhotoGallery.module 'GalleryApp.Show', (Show, PhotoGallery, Backbone, Marionette, $, _) ->

    Show.Album = Marionette.ItemView.extend
        template: '#album-show'
        tagName: 'div'
        className: 'images-wrapper container'

    Show.MissingAlbum = Marionette.ItemView.extend
        template: '#missing-album-view'

    Show.Layout = Marionette.LayoutView.extend
        template: '#album-show-layout'
        regions:
            infoRegion: '#info-region'
            imagesRegion: '#images-region'
            paginationRegion: '#pagination-region'

    Show.Info = Marionette.ItemView.extend
        template: '#album-show-info'

    Show.Pagination = Marionette.ItemView.extend
        template: '#album-show-pagination'

    undefined