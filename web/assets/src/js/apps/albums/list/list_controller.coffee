

PhotoGallery.module 'GalleryApp.List', (List, PhotoGallery, Backbone, Marionette, $, _) ->

    List.Controller =
        listAlbums: ->
            loadingView = new PhotoGallery.Common.Views.Loading()
            PhotoGallery.mainRegion.show loadingView

            promise = PhotoGallery.request 'album:entities'
            promise.then (albums) ->
                albumsListView = new List.Albums
                    collection: albums

                albumsListView.on 'childview:album:show', (childView, model) ->
                    PhotoGallery.trigger 'album:show', model.get 'id'
                    undefined

                PhotoGallery.mainRegion.show albumsListView

                undefined

            undefined

    undefined




