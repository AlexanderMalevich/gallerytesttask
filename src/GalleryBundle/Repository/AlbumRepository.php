<?php

namespace GalleryBundle\Repository;

/**
 * AlbumRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AlbumRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Get albums that includes more or equal images than $amount
     * @param int $amount - max images amount in album
     *
     * @return array
     */
    public function getAlbumsByImagesAmount($amount = 10)
    {
        try {
            $query = $this->_em->createQuery("
              SELECT a, i
              FROM GalleryBundle:Album a
              JOIN a.images i
              WHERE SIZE(a.images) <= :amount
            ");
            $query->setParameter('amount', $amount);
            return $query->getResult();
        } catch (\Exception $e) {
            return null;
        }

    }

    /**
     * Get all album images
     * @param $id - album ID
     *
     * @return \Doctrine\ORM\Query
     */
    public function getAllAlbumImages($id)
    {
        $query = $this->_em->createQuery("
            SELECT i
            FROM GalleryBundle:Image i
            WHERE i.album = :id
        ");
        $query->setParameter('id', $id);
        return $query;
    }

    /**
     * Get paginated album images
     *
     * @param $id - album ID
     * @param $page - current page
     * @return mixed|null
     */
    public function getPaginatedAlbumImages($id, $page)
    {
        try {
            $query = $this->_em->createQuery("
              SELECT a, i
              FROM GalleryBundle:Album a
              JOIN a.images i
              WHERE a.id = :id
            ");
            $query->setParameter('id', $id);
            $query->setMaxResults(10);
            $query->setFirstResult(($page - 1) * 10);
            return $query->getSingleResult();
        } catch(\Exception $e) {
            return null;
        }

    }

    public function getAlbumsWithMaxImages($imagesAmount)
    {
        try {
            /* OR
            $albumIds = $this->getAlbumsIds();
            $albums = $this->getSpecificImagesCount($albumIds, $imagesAmount);*/

            $sql = "
                   SELECT
                      a.id AS album_id,
                      a.title AS album_title,
                      a.description AS album_description,
                      a.created_at AS album_created_at,
                      GROUP_CONCAT(
                          concat('{ id: ', i.id, ', '),
                          concat(' title: ', i.title, ', '),
                          concat(' path: ', i.path, ', '),
                          concat(' created_at: ', i.created_at, ', '),
                          concat(' updated_at: ', i.updated_at, '}')
                          SEPARATOR ', ') AS images
                    FROM
                      (SELECT
                         album_id,
                         id,
                         title,
                         path,
                       	 created_at,
                       	 updated_at,
                         @rn:=CASE
                          WHEN @var_album_id = album_id THEN @rn + 1
                          ELSE 1
                          END AS rn,
                          @var_album_id:=album_id
                      FROM
                        (SELECT @var_album_id:=NULL, @rn:=NULL) vars, image
                        WHERE
                        album_id IN (SELECT id FROM album)
                      ORDER BY album_id) as i
                      INNER JOIN album a
                        on a.id = i.album_id
                    WHERE rn <= :amount
                    GROUP BY a.id
                ";
            $params = array('amount' => $imagesAmount);
            $query = $this->_em->getConnection()->prepare($sql);
            $query->execute($params);
            $albums = $query->fetchAll();
            return $albums;
        } catch(\Exception $e) {
            return null;
        }
    }

    private function getAlbumsIds()
    {
        $result = [];
        $query = $this->_em->createQuery("
          SELECT a.id FROM GalleryBundle:Album a
        ");
        foreach($query->getResult() as $row) {
            $result[] = $row['id'];
        }
        return $result;
    }

    private function getSpecificImagesCount($ids, $imagesAmount)
    {
        $result = [];
        foreach ($ids as $id) {
            $query = $this->_em->createQuery("
                  SELECT a, i
                  FROM GalleryBundle:Album a
                  JOIN a.images i
                  WHERE a.id = :album_id
                ");
            $query->setParameter('album_id', $id);
            $query->setMaxResults($imagesAmount);
            $result[] = $query->getResult();
        }
        return $result;
    }

}
